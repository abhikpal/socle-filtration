# -*- mode: python; -*-
# Socle-filtration calculator
# Copyright (C) 2019-2020 Abhik Pal
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# socles-two-diagram.py
#   socle filtrations for modules over gl_infty, sp_infty, so_infty.

from utils import *

def socle_layer_gl_mod(q, mu, nu):
    """Computes the q-th layer of the gl-module Gamma_{mu, nu}.

    See: Theorem 2.3 in [PS11].
    """
    mu_norm = sum(mu)
    nu_norm = sum(nu)

    alpha_norm = mu_norm - q + 1
    beta_norm = nu_norm - q + 1

    gammas = list(all_partitions(q - 1))
    objs = {}

    for alpha in all_partitions(alpha_norm):
        for beta in all_partitions(beta_norm):
            coeff = sum([
                lrcoef(mu, alpha, gamma) * lrcoef(nu, beta, gamma)
                for gamma in gammas
            ])
            if coeff != 0:
                sobj = tuple([tuple(alpha), tuple(beta)])
                objs[sobj] = objs.get(sobj, 0) + coeff
    return objs

def socle_layer_sp_mod(q, mu):
    """Computes the q-th layer of the sp-module Gamma_{mu, empty}.

    See: Theorem 3.3 in [PS11].
    """
    r = q - 1
    mu_norm = sum(mu)
    alpha_norm = mu_norm - 2 * r

    objs = {}

    for alpha in all_partitions(alpha_norm):
        coeff = 0
        for gamma in all_partitions(r):
            # only difference from the so case occurs here.
            gamma_2_trans = Partition([2 * gm for gm in gamma]).conjugate()
            coeff += lrcoef(mu, alpha, gamma_2_trans)
        if coeff != 0:
            sobj = (tuple(alpha),)
            objs[sobj] = objs.get(sobj, 0) + coeff

    return objs

def socle_layer_so_mod(q, mu):
    """Computes the q-th layer of the so-module Gamma_{mu, empty}.

    See: Theorem 4.3 in [PS11].
    """
    r = q - 1
    mu_norm = sum(mu)
    alpha_norm = mu_norm - 2 * r

    objs = {}

    for alpha in all_partitions(alpha_norm):
        coeff = 0
        for gamma in all_partitions(r):
            # only difference from the sp case occurs here.
            gamma_2 = Partition([2 * gm for gm in gamma])
            coeff += lrcoef(mu, alpha, gamma_2)
        if coeff != 0:
            sobj = (tuple(alpha),)
            objs[sobj] = objs.get(sobj, 0) + coeff
    return objs

def socle_filtration_gl_mod(mu, nu):
    return compute_filtration(socle_layer_gl_mod, mu, nu)

def socle_filtration_sp_mod(mu):
    return compute_filtration(socle_layer_sp_mod, mu)

def socle_filtration_so_mod(mu):
    return compute_filtration(socle_layer_so_mod, mu)

def socle_check_gl_mod(mu, nu):
    idiag = intersect_diag(mu, nu)
    top_mu = sort_diag(mu, idiag)
    top_nu = sort_diag(nu, idiag)
    check_filtration([top_mu, top_nu], sum(idiag) + 1,
                     socle_filtration_gl_mod, mu, nu)

def socle_check_sp_mod(mu):
    raise NotImplementedError
    top_diag = []
    length = 0
    check_filtration([top_diag], length, socle_filtration_sp_mod, mu)
