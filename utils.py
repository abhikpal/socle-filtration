# -*- mode: python; -*-
# Socle-filtration calculator
# Copyright (C) 2019-2020 Abhik Pal
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# utils.py

from sage.combinat.composition import Compositions
from sage.combinat.skew_partition import SkewPartition
from sage.combinat.partition import Partition

import sage.combinat.partitions as partitions
import sage.libs.lrcalc.lrcalc as lrcalc

all_partitions = partitions.ZS1_iterator
lrcoef = lrcalc.lrcoef_unsafe

def intersect_diag(mu, nu):
    """Compute the intersection of two diagrams
    """
    num_rows = min(len(mu), len(nu))
    idiag = [min(mu[k], nu[k]) for k in range(num_rows)]
    return idiag

def sort_diag(mu, nu):
    """Compute the diagram sort(mu / nu)
    """
    skew_diag = SkewPartition([mu, nu])
    sorted_diag = list(sorted(skew_diag.row_lengths(), reverse=True))
    return sorted_diag

def partition_to_str(partition):
    """Converts a partition to a string.
    """
    return ','.join(map(str, partition))

def composition_with_zero(n, k):
    """Generates unordered partitions of `n` with `k` parts.

    :param n: non-negative integer
    :param k: positive integer

    """
    for comp in Compositions(n + k, length=k):
        yield list(map(lambda x: x - 1, comp))

def compute_filtration(layer_fct, *indices):
    """A general filtration computation function.

    Uses the layer_fct to compute the filtration for the object
    parametrised by the given diagrams.

    :param layer_fct: function with signature (*diagrams, q) that
        computes the q-th socle layer
    :param indices: list of indices expected by layer_fct.

    """
    layers = []
    q = 1
    while len(layers) == 0 or len(layers[-1]) > 0:
        layers.append(layer_fct(q, *indices))
        q = q + 1
    return layers[:-1]


def check_filtration(top_diags, length, filtration_fct, *diagrams):
    """Checks the length and the top layer of the filtration.

    :param top_diags: diagrams that should appear in the topmost layer.
    :param length: expected length of the filtration
    :param filtration_fct: function to compute the filtration
    :param *diagrams: diagrams parametrising the indecomposable object

    """
    filtration = filtration_fct(*diagrams)

    flength = len(filtration)
    assert flength == length, \
        f"Expected length {length} but found {flength}."

    top_layer = filtration[-1]
    top_diags = tuple([tuple(diag) for diag in top_diags])
    assert top_layer.get(top_diags, 0) > 0, \
        f"Diagrams {top_diags} don't appear in the topmost layer {top_layer}."
