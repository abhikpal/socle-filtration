# -*- mode: python; -*-
# Socle-filtration calculator
# Copyright (C) 2019-2020 Abhik Pal
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# socles-aleph-t.py
#   socle filtrations for indecomposable injectives in T_{aleph_t}

from utils import *

def all_multi_partitions(norms):
    """Generate all partitions for the prescribed list of norms.

    :param norms: list of prescribed norms n_i
    :generates: Lists of partitions p_i where |p_i| = n_i.

    """
    if len(norms) == 0:
        yield []
    else:
        for part in all_partitions(norms[0]):
            for part_rest in all_multi_partitions(norms[1:]):
                yield [tuple(part)] + part_rest


## METHODS FOR OBJECTS (V*/V*_{aleph_u})_{lambda_u}
#
# NOTE: For these class of objects, given `u` and `lambda_u` it
# suffices to work with `t - u + 1` indices in the left-most position.
# The rest of the indices are necessarily empty. The method
# _lambda_fix_indices adds the necessary number of empty indices to
# make the format consistent with rest of the program. Moreover the
# zero-th index in the internal lists will refer to the right-most
# relevant index. This is different from the usual convention of the
# order of indices in the parameters for the simples. The
# _lambda_fix_indices function also fixes the order.

def _lambda_fix_indices(simple_idx, u, t):
    """Fix indices to be consistent with rest of the program.
    """
    num_empty = u + 2
    return tuple(list(reversed(simple_idx)) + [tuple()] * num_empty)

def _lambda_gen_simple_norms(lambda_norm, u, t):
    """Generate norms for simples.

    :param lambda_norm: |lambda_u|
    :param u:
    :param t:
    """
    for norms in composition_with_zero(lambda_norm, t - u + 1):
        yield norms

def _lambda_gen_sum_norms(simple_norms):
    """Generate norms for summation indices. 

    :param simple_norms: possible norms for the simple objects.

    """
    sum_norms = []

    if len(simple_norms) <= 1:
        return sum_norms

    for x, simple_norm in enumerate(simple_norms[1:-1], start=1):
        if x == 1:
            sum_norms.append(simple_norms[0] + simple_norm)
        else:
            sum_norms.append(simple_norm + sum_norms[-1])
    return sum_norms
        
def _lambda_filter_q(q, norms):
    """Filter norms simple that can appear in the q-th layer.

    """
    norm_sum = sum([x * eta_norm for x, eta_norm in enumerate(norms) ])
    return q == 1 + norm_sum

def _lambda_mult(lambda_u, simple_idx):
    """Computes the multiplicity of a simple object.

    """
    if len(simple_idx)  < 2:
        raise ValueError("[lambda-mult] Not enough simple objects.")
        
    if len(simple_idx) == 2:
        return lrcoef(lambda_u, simple_idx[0], simple_idx[1])
    
    simple_norms = [sum(simple) for simple in simple_idx]
    sum_norms = _lambda_gen_sum_norms(simple_norms)

    mult = 0
    for alphas in all_multi_partitions(sum_norms):
        lrc = 1
        for x, alpha in enumerate(alphas, start=1):
            if x == 1:
                N = lrcoef(alpha, simple_idx[0], simple_idx[1])
            else:
                N = lrcoef(alpha, simple_idx[x], alphas[x - 2])
            lrc = lrc * N
            if lrc == 0:
                break
        N = lrcoef(lambda_u, alphas[-1], simple_idx[-1])
        lrc = lrc * N
        mult = mult + lrc
    return mult

def _lambda_layer(q, t, u, lambda_u):
    """Compute the q-th layer.
    """

    # The t = u case is easy since only the first layer is non-empty.
    if t == u:
        if q == 1:
            return {_lambda_fix_indices([tuple(lambda_u)], u, t): 1}
        else:
            return {}

    # Generate all norms for simples and discard those that cannot
    # appear in the current layer.
    lambda_u_norm = sum(lambda_u)
    simple_norms = filter(lambda norms: _lambda_filter_q(q, norms),
                          _lambda_gen_simple_norms(lambda_u_norm, u, t))

    # Generate all possible indices for simples and compute their
    # multiplicities. Fix the indices before adding their multiplicity
    # to the current layer.
    layer = {}
    for simple_norm in simple_norms:
        for simple_idx in all_multi_partitions(simple_norm):
            mult = _lambda_mult(lambda_u, simple_idx)
            if mult != 0:
                layer[_lambda_fix_indices(simple_idx, u, t)] = mult
    return layer

def _lambda_filtration(t, u, lambda_u):
    """Compute the filtration 
    """
    f = lambda q: _lambda_layer(q, t, u, lambda_u)
    return compute_filtration(f)

def _mu_nu_check_norms(mu_norm, nu_norm, norms):
    """
    Verify if the gnerated set of norms, is correct.
    """
    etas_norm = sum(norms[:-2])
    zeta_norm = norms[-1]
    xi_norm = norms[-2]
    delta_norm = nu_norm - zeta_norm

    # print(f"* idx_sobj = {idx_sobj}, idx_sum = {idx_sum}")
    mu_err = \
        f"mu_norm mismatch."\
        f"mu_norm = {mu_norm} but got etas + xi + delta = {etas_norm} + {xi_norm} + {delta_norm}"
    assert mu_norm == etas_norm + xi_norm + delta_norm, mu_err

    nu_err = \
        f"nu_norm mismatch."\
        f"nu_norm = {nu_norm} but got delta_norm + zeta_norm = {delta_norm} + {zeta_norm}"
    assert nu_norm == delta_norm + zeta_norm, nu_err

def _mu_nu_gen_norms(mu_norm, nu_norm, t, debug=False):
    """
    Generates possible norms for the eta_j, pi_j, delta, zeta, xi
    
    output format:
        idx+norm = [eta_t, eta_(t - 1), ..., eta_0, xi, zeta]
    """
    
    ## BASE CASE. Young Lattice for t = 0.
    ##
    ##         mu -- [eta_0]
    ##         |
    ##        (pi_0) -- [xi]
    ##         |
    ##  nu -- (delta)
    ##  |
    ## [zeta]
    ##
    if t == 0:
        for pi0_norm, eta0_norm in composition_with_zero(mu_norm, 2):
            for delta_norm, xi_norm in composition_with_zero(pi0_norm, 2):
                if delta_norm > nu_norm:
                    continue
                zeta_norm = nu_norm - delta_norm
                norms = [eta0_norm, xi_norm, zeta_norm]
                if debug:
                    _mu_nu_check_norms(mu_norm, nu_norm, idx_sobj, idx_sum)
                yield norms
                
    ## INDUCTIVE CASE. Young for t > 0. Note that we can consider the lattice
    ## rooted at pi_t as a new lattice for t - 1 with mu' = pi_t. We can then 
    ## recurse.
    ##
    ##         mu -- [eta_t]
    ##         |
    ##        (pi_t) -- [eta_(t - 1)]
    ##         |
    ##        ...       ...
    ##         |
    ##        (pi_1) -- [eta_0]
    ##         |
    ##        (pi_0) -- [xi]
    ##         |
    ##  nu -- (delta)
    ##  |
    ## [zeta]   
    elif t > 0:
        for pi_t_norm, eta_t_norm in composition_with_zero(mu_norm, 2):
            for norms in _mu_nu_gen_norms(pi_t_norm, nu_norm, t - 1):
                yield [eta_t_norm] + norms
    ## Catch potential typos:
    else:
        raise ValueError("Incorrect t value")

def _mu_nu_filter_q(mu_norm, nu_norm, q, norms):
    """
    Filters norms that can appear in the q-th level. 
    
    :norms: format similar to the output of _mu_nu_gen_norms.
    """
    eta_norms = reversed(norms[:-2])
    zeta_norm = norms[-1]
    
    eta_sum = sum([eta_x * (x + 1) for x, eta_x in enumerate(eta_norms)])
    return q == 1 + nu_norm - zeta_norm + eta_sum
def _mu_nu_gen_pi_norms(mu_norm, nu_norm, norms):
    t = len(norms) - 3
    zeta_norm = norms[-1]
    xi_norm = norms[-2]
    delta_norm = nu_norm - zeta_norm
    pi_norms = [delta_norm + xi_norm]
    eta_norms = list(reversed(norms[:-2]))
    for x in range(1, t + 1):
        pi_norms.append(pi_norms[x - 1] + eta_norms[x - 1])
    
    
#     zeta_norm = norms[-1]
#     xi_norm = norms[-2]
#     delta_norm = nu_norm - zeta_norm
#     pi_norms = [delta_norm + xi_norm]
#     for x, eta_x in enumerate(reversed(norms[1:-2])):
#         pi_norms.append(pi_norms[x - 1] + eta_x)
    return pi_norms

def _mu_nu_simple_mult(mu, nu, simple_idx):
    """Compute the multiplicity of th esimple object that appears in the mu-nu subquotient
    """
    zeta = simple_idx[-1] 
    xi = simple_idx[-2]
    etas = list(reversed(simple_idx[:-2]))
    
    delta_norm = sum(nu) - sum(zeta)
    norms = [sum(idx) for idx in simple_idx]
    pi_norms = _mu_nu_gen_pi_norms(sum(mu), sum(nu), norms)

    mult = 0 
    for delta in all_partitions(delta_norm):
        lrc = 1
        del_lrc = lrcoef(nu, zeta, delta)
        if del_lrc == 0:
            continue
        for pis in all_multi_partitions(pi_norms):
            lrc = 1
            # print(pis + [delta])
            # print(f"nu, zeta, delta: \t{del_lrc}\t | LR({nu}, {zeta}, {delta})")
            for x, pi_x in enumerate(pis):
                if x == 0:
                    lr = lrcoef(pi_x, xi, delta)
                    # print(f"pi0, xi, delta: \t{lr}\t | LR({pi_x}, {xi}, {delta})")
                    lrc = lr * lrc
                else:
                    lr = lrcoef(pi_x, pis[x - 1], etas[x - 1])
                    # print(f"pi{x}, pi{x - 1}, eta{x - 1}: \t{lr}\t | LR({pi_x}, {pis[x - 1]}, {etas[x - 1]})")
                    lrc = lr * lrc
                    
                if lrc == 0:
                    continue

            lr = lrcoef(mu, pis[-1], etas[-1]) * del_lrc
            lrc = lr * lrc
            # print(f"mu, pi_t, eta_t: \t{lr}\t | LR({mu}, {pis[-1]}, {etas[-1]})")
            # print("-" * 10)
            # print()
            mult = mult + lrc
    return mult

def _aleph_layer_mu_nu(q, t, mu, nu):
    mu_norm = sum(mu)
    nu_norm = sum(nu)
    
    # 1. Filter the norms that are possible in this layer.
    simple_norms = list(filter(lambda norms: _mu_nu_filter_q(mu_norm, nu_norm, q, norms),
                              _mu_nu_gen_norms(mu_norm, nu_norm, t)))
    subq = {}
    for simple_norm in simple_norms:
        # print(f"NORM: {simple_norm}")
        for simple_idx in all_multi_partitions(simple_norm):
            mult = _mu_nu_simple_mult(mu, nu, simple_idx)
            # print(f"\t IDX: {simple_idx}, MUL: {mult}")
            if mult != 0:
                subq[tuple(simple_idx)] = mult
    return subq

def _aleph_filtration_mu_nu(t, mu, nu):
    f = lambda q: _aleph_layer_mu_nu(q, t, mu, nu)
    return compute_filtration(f)

def _tensor_lambdas(idx_1, idx_2): # TODO: rename to _tensor_lambdas
    """
    Assumes: both idx_1 and idx_2 have the same length
    """
    if len(idx_1) == 1:
        tensor_data =  lrcalc.mult(idx_1[0], idx_2[0])
        tensor = {}
        for idx, mult in tensor_data.items():
            tensor[tuple([idx])] = mult
        return tensor
    else:
        tensor_rest = _tensor_lambdas(idx_1[1:], idx_2[1:])
        tensor_current = lrcalc.mult(idx_1[0], idx_2[0])
        tensor = {}
        for obj_cur, mult_cur in tensor_current.items():
            for obj_res, mult_res in tensor_rest.items():
                idx = tuple([obj_cur] + list(obj_res))
                mult = mult_cur * mult_res
                tensor[idx] = mult
        return tensor

def _tensor_simples(simple_1, simple_2):
    """Compute the tensor of two simple objects
    """
    mu_1, nu_1 = simple_1[-2], simple_1[-1]
    mu_2, nu_2 = simple_2[-2], simple_2[-1]
    
    if len(mu_1) + len(nu_1) > 0 and len(mu_2) + len(nu_2) > 0:
        raise ValueError("Unable to tensor!")
    elif len(mu_1) + len(nu_1) > 0:
        mu, nu = mu_1, nu_1
    else:
        mu, nu = mu_2, nu_2
    
    lambdas = _tensor_lambdas(simple_1[:-2], simple_2[:-2])
    return { ky + (Partition(mu), Partition(nu)) : mult for ky, mult in lambdas.items()}
    
def _tensor_sums_base(sum_1, sum_2):
    """
    Compute the tensor product of two direct sums. Each sum is a dictionary of the form
        { object : multiplicity }
    """
    
    tensor = {}
    for simple_1, mult_1 in sum_1.items():
        for simple_2, mult_2 in sum_2.items():
            new_mult = mult_1 * mult_2
            new_summands = _tensor_simples(simple_1, simple_2)
            for summand, smult in new_summands.items():
                tensor[summand] = tensor.get(summand, 0) + new_mult * smult
    return tensor

def _tensor_sums(sums):
    """Compute the tensor of an arbitrary list of direct sums.
    
    `sums` is assumes to be list of dictionaries.
    """
    if len(sums) == 0:
        return {}
    elif len(sums) == 1:
        return sums[0]
    else:
        return _tensor_sums([_tensor_sums_base(sums[0], sums[1])] + sums[2:])


def socle_filtration_aleph_t(*indices):
    t = len(indices) - 3        
    if t < 0:
        raise ValueError("Insufficient number of indices.")

    ## COMPUTE THE FILTRATIONS SEPARATELY
    # Convention: the index (-1) will denote the mu-nu object in the internal dictionary
    fsep = { t - u: _lambda_filtration(t, t - u, indices[u]) for u in range(t, -1, -1)}
    fsep[-1] = _aleph_filtration_mu_nu(t, indices[-2], indices[-1])

    layers = []
    q = 0
    while len(layers) == 0 or len(layers[-1]) > 0:
        # At the start the q-th socle layer is emtpy
        layer = dict()

        ## COMPUTE INDICES u_x and y.
        # The condition on the indices implies that sum(u_x) + y = q + t + 1.
        q = q + 1
        for us in Compositions(1 + q + t, length=t + 2):
            # we can index us by -1, 0, 1, ..., t. And then assume that u_(-1) = y.
            # We only need to tensor when *all* objects have non-zero socles at the layers prescribed by idx.
            all_non_zero = all([ux <= len(fsep[x]) for x, ux in enumerate(us, start=-1)])
            tensorands = []
            if all_non_zero:
                for x, ux in enumerate(us, start=-1):
                    tensorands.append(fsep[x][ux - 1])
            else:
                continue

            # We now tensor the objects and add them to the layer
            for obj, mult in _tensor_sums(tensorands).items():
                if sum([sum(idx) for idx in obj]) != 0:
                    layer[obj] = layer.get(obj, 0) + mult
        layers.append(layer)
    return layers[:-1]
