# -*- mode: python; -*-
# Socle-filtration calculator
# Copyright (C) 2019-2020 Abhik Pal
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# utils_latex.py
#   Utilities to generate LaTeX-ed tables, etc

from collections import namedtuple

from sage.misc.latex import LatexExpr

from utils import *

Parens =  namedtuple('Parens', ['l', 'r'])
PARS_NONE = Parens("", "")
PARS_PARENS = Parens(r'\left(', r'\right)')
PARS_SQUARE = Parens(r'\left[', r'\right]')
PARS_CURLED = Parens(r'\left\{', r'\right\}')
PARS_ANGLED  = Parens(r'\left\langle', r'\right\rangle')

LABEL_TWO_DIAG = r"\Gamma"
LABEL_ALEPH_T = r"V"

EMPTY_SET = r"\emptyset"
EMPTY_SET = r"\varnothing"

def detect_latex_expr(lfunc):
    """Modifies functions so that they output LatexExpr by default.

    This is useful because then one sees nicely formatted LaTeX output
    when functions are called directly from the notebook.

    """
    def _lfunc(*args, **kwargs):
        expr = kwargs.pop('expr', True)
        if expr:
            return LatexExpr(lfunc(*args, **kwargs))
        else:
            return lfunc(*args, **kwargs)
    return _lfunc

@detect_latex_expr
def partition_to_latex(partition, delim=r",\,"):
    """Convert a single partition to latex

    :param partition:
    :param delim: Delimeter to separate the various parts

    """
    if len(partition) == 0:
        return EMPTY_SET
    return PARS_PARENS.l + delim.join(map(str, partition)) + PARS_PARENS.r

@detect_latex_expr
def indices_to_latex(*indices, idelim=r", \,", pdelim=r",\,", brackets=PARS_NONE):
    """Convert (multiple) indices of partitions to a LaTeX

    :param indices: Indices to be converted
    :param idelim: Delimeter between two indices
    :param pdelim: Delimeter to use insider partitions.
    :param brackets: Type of brackets to use around the list of indices

    """
    p2l = lambda p: partition_to_latex(p, delim=pdelim, expr=False)
    return brackets.l + idelim.join(map(p2l, list(indices))) + brackets.r

@detect_latex_expr
def latex_sobj(sobj, mult, mtype):
    """Typeset one simple object with the given multiplicity.

    :param sobj:
        List of indices parametrising the simple object.

    :param mult:
        Multiplicty of the given object.

    :param mtype:

        Type of module. Should be one of 'gl', 'sp', 'so' or
        'aleph_t'. In each of these cases:

        - 'gl': object is labeled as "Gamma_{mu; nu}"
        - 'sp': object is labeled as "Gamma_{<mu>}"
        - 'so': object is labeled as "Gamma_{[mu]}"
        - 'aleph_t': object is labled as "V_{lambda_t, ..., lambda_0, mu, nu}"
    """

    if mult == 0:
        return ""

    label = LABEL_ALEPH_T if mtype == 'aleph_t' else LABEL_TWO_DIAG
    mult_str = "" if mult == 1 else str(mult)

    idelims = {
        'aleph_t': r",\,",
        'gl': r";\,",
    }
    idelim = idelims.get(mtype, r"\,")

    brackets = {
        'so': PARS_SQUARE,
        'sp': PARS_ANGLED,
        'gl': PARS_NONE,
        'aleph_t': PARS_NONE
    }
    bracket = brackets.get(mtype, PARS_PARENS)

    sobj_str = f"{mult_str}\\,{label}_" + \
        "{" + indices_to_latex(*sobj, idelim=idelim, brackets=bracket, expr=False) + "}"

    return sobj_str

@detect_latex_expr
def latex_layer(layer, mtype):
    """Typeset one layer of a socle filtration.

    :param layer:
        The layer of the socle filtration as a dictionary of {
        simple_object: multiplicity } values.

    :param mtype:
        Type of module. See description in `latex_sobj`

    :param expr:
        Toggles whether the output should be a LatexExpr. (defaults to
        True)

    """
    sobj_list = []
    for sobj in sorted(layer.keys(), key=partition_to_str):
        sobj_list.append(latex_sobj(sobj, layer[sobj], mtype, expr=False))
    layer_str = "\\oplus".join(sobj_list)
    return layer_str

@detect_latex_expr
def latex_filtration(filtration, mtype):
    """Typeset a socle filtration

    :param filtration:
    :param mtype:
        Type of module. See description in `latex_sobj`

    """

    thead = \
        r"\begin{array}{|c|}" \
        r"\hline" "\n"

    ttail = \
        r"\\ \hline" \
        r"\end{array}"

    math_begin = ''

    math_end = ''

    row_sep = math_end + "\\\\ \\hline\n " + math_begin
    llayer = lambda l: latex_layer(l, mtype, expr=False)
    tbody = row_sep.join(map(llayer, reversed(filtration)))

    return thead + math_begin + tbody + math_end + ttail
