# -*- mode: python; -*-
# Socle-filtration calculator
# Copyright (C) 2019-2020 Abhik Pal
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

# socles.py
#   Utilities to generate LaTeX-ed tables, etc. Outputs latex by
#   default and is optimized for use in notebooks.

from socles_two_diag import socle_filtration_gl_mod
from socles_two_diag import socle_filtration_so_mod
from socles_two_diag import socle_filtration_sp_mod
from socles_two_diag import socle_layer_gl_mod
from socles_two_diag import socle_layer_so_mod
from socles_two_diag import socle_layer_sp_mod

from utils_latex import latex_filtration
from utils_latex import latex_layer

def socle_filtration(mtype, *indices):
    if mtype == 'aleph_t':
        raise NotImplementedError
    elif mtype == 'gl':
        filtration = socle_filtration_gl_mod(*indices)
    elif mtype == 'sp':
        filtration = socle_filtration_sp_mod(*indices)
    elif mtype == 'so':
        filtration = socle_filtration_so_mod(*indices)
    else:
        raise ValueError("Module type not understood")

    return latex_filtration(filtration, mtype)

def socle_layer(mtype, q, *indices):
    if mtype == 'aleph_t':
        raise NotImplementedError
    elif mtype == 'gl':
        filtration = socle_layer_gl_mod(q, *indices)
    elif mtype == 'sp':
        filtration = socle_filtration_sp_mod(q, *indices)
    elif mtype == 'so':
        filtration = socle_filtration_so_mod(q, *indices)
    else:
        raise ValueError("Module type not understood")

    return latex_layer(filtration, mtype)
